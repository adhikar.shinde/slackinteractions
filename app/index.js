const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
var request = require("request");
const fetch = require("node-fetch");

app.listen(4000, () => {
  console.log("app running on port 4000");
});
app.get("/", async (req, res) => {
  res.send("Hello");
});

app.post("/slack/interaction", async (req, res) => {
  var slack_payload = JSON.parse(req.body.payload);
  // console.log("%o", { slack_payload });

  let { actions, user, message, response_url } = slack_payload;
  // console.log({ user });
  console.log("%o", { message });
  console.log("%o", message.blocks[2].elements);
  console.log("%o", actions);
  // let block1 = message.blocks[1];
  console.log({ user });
  let container_result = await fetch(response_url, {
    method: "post",
    body: JSON.stringify({
      ...message,
      response_type: "ephemeral",
      replace_original: true,
    }),

    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer xoxb-4457725250951-4472280615986-Jll3p6D2yP9UJ4ZJ6m35aREK",
    },
  });
  console.log({ response_url });
  const data = await container_result.json();

  console.log(data);

  res.sendStatus(200);
});
